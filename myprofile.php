<?php
session_start();
error_reporting(0);
include('includes/config.php');
if (strlen($_SESSION['alogin']) == 0) {
    header('location: ../../index.php');
} else {
    $eid = $_SESSION['alogin'];
    if (isset($_POST['update'])) {

        $fname = $_POST['firstName'];
        $gender = $_POST['gender'];
        $dob = $_POST['dob'];
        $department = $_POST['department'];
        $department_text = $_POST['department_text'];
        $address = $_POST['address'];
        $alocatedLeave = $_POST['alocatedLeave'];
        $mobileno = $_POST['mobileno'];
        $sql = "update user set name=:fname,gender=:gender,Birthdate=:dob,department=:department_text,Address=:address,department_id=:department,mobile=:mobileno,allocated_leave=:alocatedLeave where email=:eid";
        $query = $dbh->prepare($sql);
        $query->bindParam(':fname', $fname, PDO::PARAM_STR);
        $query->bindParam(':gender', $gender, PDO::PARAM_STR);
        $query->bindParam(':dob', $dob, PDO::PARAM_STR);
        $query->bindParam(':department', $department, PDO::PARAM_STR);
        $query->bindParam(':department_text', $department_text, PDO::PARAM_STR);
        $query->bindParam(':address', $address, PDO::PARAM_STR);
        $query->bindParam(':alocatedLeave', $alocatedLeave, PDO::PARAM_STR);
        $query->bindParam(':mobileno', $mobileno, PDO::PARAM_STR);
        $query->bindParam(':eid', $eid, PDO::PARAM_STR);

        try {
            $res =  $query->execute();
            if ($res) {
                $msg = "Employee record updated Successfully";
            } else {
                $error = "Something went wrong";
            }
            echo "<script>console.log('Debug department: " .    $department_text . "' );</script>";
        } catch (PDOException $e) {
            echo "<script>console.log('Debug Objects: " .   $et . "' );</script>";
            $msg = $e;
        }
    }

?>

    <!DOCTYPE html>
    <html lang="en">

    <head>

        <!-- Title -->
        <title>Admin | Update Employee</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />

        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="assets/plugins/materialize/css/materialize.min.css" />
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">
        <link href="assets/css/alpha.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
        <style>
            .errorWrap {
                padding: 10px;
                margin: 0 0 20px 0;
                background: #fff;
                border-left: 4px solid #dd3d36;
                -webkit-box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
                box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
            }

            .succWrap {
                padding: 10px;
                margin: 0 0 20px 0;
                background: #fff;
                border-left: 4px solid #5cb85c;
                -webkit-box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
                box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .1);
            }
        </style>





    </head>

    <body>
        <?php include('includes/header.php'); ?>

        <?php include('includes/sidebar.php'); ?>
        <main class="mn-inner">
            <div class="row">
                <div class="col s12">
                    <div class="page-title">Update employee</div>
                </div>
                <div class="col s12 m12 l12">
                    <div class="card">
                        <div class="card-content">
                            <form id="example-form" method="post" name="updatemp">
                                <div>
                                    <h3>Update Employee Info</h3>
                                    <?php if ($error) { ?><div class="errorWrap">
                                            <strong>ERROR</strong>:<?php echo htmlentities($error); ?>
                                        </div><?php } else if ($msg) { ?><div class="succWrap"><strong>SUCCESS</strong> : <?php echo htmlentities($msg); ?>
                                        </div><?php } ?>
                                    <section>
                                        <div class="wizard-content">
                                            <div class="row">
                                                <div class="col m6">
                                                    <div class="row">
                                                        <?php
                                                        $eid = $_SESSION['alogin'];
                                                        $sql = "SELECT * FROM user WHERE email=:eid";
                                                        $query = $dbh->prepare($sql);
                                                        $query->bindParam(':eid', $eid, PDO::PARAM_STR);
                                                        $query->execute();
                                                        $results = $query->fetchAll(PDO::FETCH_OBJ);
                                                        $cnt = 1;
                                                        if ($query->rowCount() > 0) {
                                                            $first_value = reset($results);
                                                            echo "<script>console.log('Debug : " .  $first_value->role_id  . "' );</script>";
                                                            foreach ($results as $result) {               ?>
                                                                <div class="input-field col m6  s12">
                                                                    <label for="empcode">Employee Code</label>
                                                                    <input disabled name="empcode" id="empcode" value="<?php echo htmlentities($result->id); ?>" type="text" autocomplete="off" readonly required>
                                                                    <span id="empid-availability" style="font-size:12px;"></span>
                                                                </div>
                                                                <div class="input-field col m6 s12">
                                                                    <label for="firstName">Full Name</label>
                                                                    <input id="firstName" name="firstName" value="<?php echo htmlentities($result->name); ?>" type="text" required>
                                                                </div>

                                                                <div class="input-field col m6 s12">
                                                                    <label for="email">Email</label>
                                                                    <input disabled name="email" type="email" id="email" value="<?php echo htmlentities($result->email); ?>" readonly autocomplete="off" required>
                                                                    <span id="emailid-availability" style="font-size:12px;"></span>
                                                                </div>
                                                                <div class="input-field col m6 s12">
                                                                    <label for="mobileno">Mobile number</label>
                                                                    <input id="mobileno" name="mobileno" type="tel" value="<?php echo htmlentities($result->mobile); ?>" maxlength="10" autocomplete="off" required>
                                                                </div>
                                                                <div class="input-field col m6 s12 ">
                                                                    <label for="alocatedLeave">Allocated Leave </label>
                                                                    <input id="alocatedLeave" name="alocatedLeave" value="<?php echo htmlentities($result->allocated_leave); ?>" type="text" autocomplete="off" required readonly>
                                                                </div>

                                                    </div>
                                                </div>

                                                <div class="col m6">
                                                    <div class="row">
                                                        <div class="input-field col m6 s12">
                                                            <select name="gender" autocomplete="off">
                                                                <?php echo ("<OPTION value=$result->gender>  $result->gender</option>"); ?>
                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                        </div>
                                                        <div class="input-field col m6 s12">
                                                            <label for="birthdate">Date of Birth</label>
                                                            <input id="dob" name="dob" class="datepicker" value="<?php echo htmlentities($result->Birthdate); ?>">
                                                        </div>
                                                        <div class="input-field col m6 s12">
                                                            <SELECT id="department" name="department" onchange="getText(this)">
                                                                <?php echo ("<OPTION VALUE=$resultt->department_id>  $result->department</option>"); ?>
                                                                <?php
                                                                $sql = "SELECT * from department";
                                                                $query = $dbh->prepare($sql);
                                                                $query->execute();
                                                                $res = $query->fetchAll(PDO::FETCH_OBJ);
                                                                $cnt = 1;
                                                                $options = "";
                                                                if ($query->rowCount() > 0) {
                                                                    foreach ($res as $resultt) {   ?>
                                                                        <option value="<?php echo htmlentities($resultt->id); ?>"><?php echo htmlentities($resultt->Department); ?></option>


                                                                <?php }
                                                                } ?>
                                                            </SELECT>
                                                            <input type="hidden" name="department_text" id="department_text">
                                                            <input type="hidden" name="department_val" id="department_val">
                                                            <script>
                                                                $(document).ready(function() {
                                                                    $("#department").onchange(function() {
                                                                        // $("#department_text").val(("#department").find(":selected").text());
                                                                        var textVal = document.getElementById("department").text;
                                                                        document.getElementById("department_val").value = textVal;
                                                                    });
                                                                });

                                                                function getText(element) {
                                                                    var textHolder = element.options[element.selectedIndex].text;
                                                                    var textVal = element.options[element.selectedIndex].value;

                                                                    document.getElementById("department_text").value = textHolder;
                                                                    document.getElementById("department_val").value = textVal;

                                                                    console.log(textHolder);
                                                                    console.log(textVal);
                                                                }
                                                            </script>
                                                        </div>
                                                        <div class="input-field col m6 s12">
                                                            <label for="address">Address</label>
                                                            <input id="address" name="address" type="text" value="<?php echo htmlentities($result->Address); ?>" autocomplete="off" required>
                                                        </div>
                                                <?php }
                                                        } ?>

                                                <div class="input-field col s12">
                                                    <button type="submit" name="update" id="update" class="waves-effect waves-light btn indigo m-b-xs">UPDATE</button>

                                                </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>


                                    </section>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        </div>
        <div class="left-sidebar-hover"></div>

        <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="assets/plugins/material-preloader/js/materialPreloader.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="assets/js/alpha.min.js"></script>
        <script src="assets/js/pages/form_elements.js"></script>

    </body>

    </html>
<?php } ?>